# NoteTk
## A simple CLI note taking app written in Go :)

```
Usage: notetk <command> <args>

Commands:   
    -c, --config         Configures the default editor and note location
    -n, --new <name>     Creates new note
    -l, --list           List all notes"
    -d, --delete         Deletes note
    -h, --help           Prints this help message :)
```

### Requires
- fzf

### Install
Just run the `install.sh` script
