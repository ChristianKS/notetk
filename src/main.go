package main

import (
	"bufio"
	"fmt"
	"os"
	"os/exec"
	"strings"
)

func main() {
	args := os.Args

    if len(args) <= 1 {
        List()
        return
    }

    switch args[1] {
        case "-l", "--list": List()

        case "-c", "--config":
            Config()

        case "-n", "--new": 
            if len(args) <= 2 {
                Help("new")
                return
            }

            New(args[2])

        case "-h", "--help": Help("default")

        case "-d", "--delete": Delete()
    }
}

func Help(code string) {
    switch code {
        case "default": fmt.Print("A simple CLI note taking app :)\nUsage: notetk <command> <args>\n\nCommands:\n   -c, --config         Configures the default editor and note location\n   -n, --new <name>     Creates new note\n   -l, --list           List all notes\n   -d, --delete         Deletes note\n   -h, --help           Prints this help message :)")
        case "new": fmt.Print("Usage: notetk -n <name>     Ex: notetk -n mynote or notetk --new myfolder/mynote")
    }
}

func Delete() {
    path, _ := Get_vars()

    cmd := exec.Command("find", path, "-type", "f")

    output, err := cmd.Output()
    check(err)

    fzfCmd := exec.Command("fzf")
    fzfCmd.Stderr = os.Stderr

    stdin, err := fzfCmd.StdinPipe()
    check(err)

    _, stdinerr := stdin.Write(output)
    check(stdinerr)

    selected, err := fzfCmd.Output()
    check(err)

    selectedPath := strings.TrimSpace(string(selected))

    var answer string
    fmt.Print("Do you want to delete " + selectedPath + "? (y/n): ")
    fmt.Scanln(&answer)

    if answer == "y" {
        err = os.Remove(selectedPath)
        check(err)
    }
}

func New(name string) {
    path, editor := Get_vars()

    parts := strings.Split(name, "/")
    if len(parts) > 1 {
        current_path := path
        for i := 0; i < len(parts) - 1; i++ {
            err := os.Mkdir(current_path + "/" + parts[i], os.ModePerm)
            check(err)

            current_path = current_path + "/" + parts[i]
        }
        path = current_path
        name = parts[len(parts) - 1]
    }

    _, err := os.Create(path + "/" + name + ".md")
    check(err)

    editor_cmd := exec.Command(editor, path + "/" + name + ".md")
    editor_cmd.Stdin = os.Stdin
    editor_cmd.Stdout = os.Stdout
    editor_cmd.Stderr = os.Stderr

    err = editor_cmd.Run();
    check(err)

    fmt.Println("Note saved at: " + path + "/" + name + ".md")
}

func Config() {
    original_path, original_editor := Get_vars();
    home, err := os.UserHomeDir();
    check(err);

    config_path := home + "/.config/note/config";

    if _, err := os.Stat(config_path); err != nil {
        err = os.Mkdir(home + "/.config/note", os.ModePerm)
        check(err)
        _, err = os.Create(config_path)
        check(err)
    }

    var path, editor string
    fmt.Print("Enter path (leave blank if same): ")
    fmt.Scanln(&path)
    if path == "" {
        path = original_path
    } else {
        if strings.Contains(path, "~") {
            path = strings.Replace(path, "~", home, 1)
        }

        err = os.MkdirAll(path, os.ModePerm)
    }
    check(err)
    fmt.Print("Enter editor (leave blank if same): ")
    fmt.Scanln(&editor)
    if editor == "" {
        editor = original_editor
    }

    err = os.WriteFile(config_path, []byte("path=" + path + "\neditor=" + editor + "\n"), 0644);
    check(err)
}

func Get_vars() (string, string) {
    home, err := os.UserHomeDir();
    check(err);
    path := home + "/.config/note/config";
    note_path := home + "/notes";
    editor := "vi";

    editor_env := os.Getenv("EDITOR")
    if editor_env != "" {
        editor = editor_env
    }

    if _, err := os.Stat(path); err != nil {
    } else {
        file, err := os.Open(path);
        check(err);
        defer file.Close()

        scanner := bufio.NewScanner(file);

        for scanner.Scan() {
            parts := strings.Split(scanner.Text(), "=")
            switch parts[0] {
            case "path":
                if parts[1] != "" {
                    note_path = parts[1]
                }
            case "editor":
                if parts[1] != "" {
                    editor = parts[1]
                }
            }
        }
    }

    if _, err := os.Stat(note_path); err != nil {
        if os.IsNotExist(err) {
            current_path := note_path
            parts := strings.Split(note_path, "/")
            for i := 0; i < len(parts) - 1; i++ {
                err := os.Mkdir(note_path, os.ModePerm)
                check(err)
                current_path = current_path + "/" + parts[i]
            }
            note_path = current_path
        }
    }

    return note_path, editor;
}

func List() {
    path, editor := Get_vars()

	cmd := exec.Command("find", path, "-type", "f")

	output, err := cmd.Output()
    check(err)

	fzfCmd := exec.Command("fzf")
	fzfCmd.Stderr = os.Stderr

	stdin, err := fzfCmd.StdinPipe()
    check(err)

	_, stdinerr := stdin.Write(output)
    check(stdinerr)

	selected, err := fzfCmd.Output()
    check(err)

	selectedPath := strings.TrimSpace(string(selected))

    if selectedPath != "" {
		editor_cmd := exec.Command(editor, selectedPath)
		editor_cmd.Stdin = os.Stdin
		editor_cmd.Stdout = os.Stdout
		editor_cmd.Stderr = os.Stderr

		 err = editor_cmd.Run();
         check(err)

         fmt.Println("Note saved at: " + selectedPath)
	} else {
		fmt.Println("No note selected.")
	}
}

func check(e error) {
    if e != nil {
        return
    }
}
